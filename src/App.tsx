import {
  IonApp,
  IonCol,
  IonContent,
  IonPage,
  IonRouterOutlet,
  IonRow,
  IonSplitPane,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Route } from "react-router-dom";

import ArticleList from "./components/articles/views/List";
import ArticlePreview from "./components/articles/views/Preview";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "@fontsource/amatic-sc"
import "@fontsource/roboto"

import "./theme/variables.css";

const App: React.FC = () => {

  return (
    <IonApp id="app">
      <IonReactRouter>
        <IonSplitPane contentId="main" when="null">

          <IonRouterOutlet id="main">
            <IonPage>
              {/* <Head /> */}

               <IonContent fullscreen>
                {/* <IonRow id="background-content">
                  <IonCol offsetMd="2" sizeMd="8"> */} 

                    <Route path="/article" exact={true}>
                      <ArticleList />
                    </Route>

                    <Route path="/preview/" exact={true}>
                      <ArticlePreview />
                    </Route>

                  {/* </IonCol>
                </IonRow> */}
              </IonContent>
            </IonPage>
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
