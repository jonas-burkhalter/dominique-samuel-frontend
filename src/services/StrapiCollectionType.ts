export default interface StrapiCollectionType<T> {
  data: Array<{
    attributes: T;
    id: Number;
  }>;
  meta: Object;
}
