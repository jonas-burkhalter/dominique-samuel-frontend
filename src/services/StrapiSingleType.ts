export default interface StrapiSingleType<T> {
  data: {
    attributes: T;
    id: Number;
  };
  meta: Object;
}
