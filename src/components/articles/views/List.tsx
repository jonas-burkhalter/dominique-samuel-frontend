import { IonCol, IonRow } from "@ionic/react";
import { useEffect, useState } from "react";
import ArticleCard from "../components/ArticleCard";
import GiverView from "../components/GiverView";

import Article from "../models/Article";
import { articleService } from "../services/ArticleService";

const ArticleView: React.FC = () => {

  const [articles, setArticles] = useState<Array<Article>>([]);
  const [id, setId] = useState<number>(null);

  useEffect(() => {
    articleService.getAll().then((result) => {
      setArticles(result.data.map((a) => {
        return {
          category: a.attributes.category,
          description: a.attributes.description,
          givers: a.attributes.givers,
          id: a.id,
          image: a.attributes.image,
          price: a.attributes.price,
          slug: a.attributes.slug,
          title: a.attributes.title
        }
      }));
    });
  }, []);

  function compareFn(a1: Article, a2: Article) {
    if (a1.givers.data.length > a2.givers.data.length) {
      return 1;
    }
    if (a1.givers.data.length < a2.givers.data.length) {
      return -1;
    }

    if (a1.slug > a2.slug) {
      return 1;
    }
    if (a1.slug < a2.slug) {
      return -1;
    }

    // a must be equal to b
    return 0;
  }
  
  function onClose() {
    setId(null)
  }

  function onOpen(id: number) {
    setId(id)
  }

  return (
    <IonRow>
      {articles?.sort(compareFn).map((article) => (
        <>
          <IonCol key={`article-card-${article.id}`} size="12" sizeMd="6">
            <ArticleCard article={article} onOpen={(id) => onOpen(id)} />
          </IonCol>

          {(id !== null && article.id === id) && (
            <GiverView article={article} key={`giver-${article.id}`} onClose={onClose} />
          )}
        </>
      ))}
    </IonRow>
  );
};

export default ArticleView;
