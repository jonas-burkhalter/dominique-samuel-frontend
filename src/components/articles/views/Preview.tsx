import { IonCol, IonRow } from "@ionic/react";
import { useEffect, useState } from "react";
import ArticleCard from "../components/ArticleCard";

import Article from "../models/Article";
import { articleService } from "../services/ArticleService";

const ArticleView: React.FC = () => {

  const [articles, setArticles] = useState<Array<Article>>([]);

  useEffect(() => {
    articleService.getAll().then((result) => {
      setArticles(result.data.map((a) => {
        return {
          category: a.attributes.category,
          description: a.attributes.description,
          givers: a.attributes.givers,
          id: a.id,
          image: a.attributes.image,
          price: a.attributes.price,
          slug: a.attributes.slug,
          title: a.attributes.title
        }
      }));
    });
  }, []);

  function onOpen(id: number) {
    window.location.replace("https://www.dominique-samuel.ch/wunschliste");
  }

  return (
    <IonRow>
      {articles?.filter(a => a.givers.data.length === 0).sort( () => .5 - Math.random() ).slice(0, 2).map((article) => (
        <>
          <IonCol key={`article-card-${article.id}`} size="12" sizeMd="6">
            <ArticleCard article={article} onOpen={(id) => onOpen(id)}  />
          </IonCol>
        </>
      ))}
    </IonRow>
  );
};

export default ArticleView;
