import StrapiCollectionType from "../../../services/StrapiCollectionType";
import StrapiSingleType from "../../../services/StrapiSingleType";
import Article from "../models/Article";

export interface IArticleService {
  getAll(): Promise<StrapiCollectionType<Article>>;
  get(slug:string): Promise<StrapiSingleType<Article>>;
}