import BaseServie from "../../../services/BaseService";
import StrapiCollectionType from "../../../services/StrapiCollectionType";
import Article from "../models/Article";
import { IArticleService } from "./ArticleServiceInterface";
import qs from "qs";
import StrapiSingleType from "../../../services/StrapiSingleType";

export default class ArticleService
  extends BaseServie
  implements IArticleService {
  private RESSOURCE_URL: string;

  constructor(base_url: string = "") {
    super();
    console.debug("articleService::constructor", base_url);

    this.RESSOURCE_URL = `${this.BASE_URL}/articles`;
  }

  public async getAll(): Promise<StrapiCollectionType<Article>> {
    const query = qs.stringify(
      {    
        populate: ['image', 'givers'],
      },
      {
        encodeValuesOnly: true,
      }
    );

    return super.GET(`${this.RESSOURCE_URL}/?${query}`);
  }

  public async get(slug: string): Promise<StrapiSingleType<Article>> {
    const query = qs.stringify(
      {
        fields: [
          'description',
          'title'
        ],
      },
      {
        encodeValuesOnly: true,
      }
    );

    return super.GET(`${this.RESSOURCE_URL}/?${query}`);
  }
}

export const articleService = new ArticleService();
