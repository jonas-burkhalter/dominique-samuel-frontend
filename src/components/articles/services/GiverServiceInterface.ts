import Giver from "../models/Giver";

export interface IGiverService {
  create(value: Giver): Promise<void>;
}