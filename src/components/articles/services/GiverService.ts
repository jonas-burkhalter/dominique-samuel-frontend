import BaseServie from "../../../services/BaseService";
import Giver from "../models/Giver";
import { IGiverService } from "./GiverServiceInterface";

export default class GiverService
  extends BaseServie
  implements IGiverService {
  private RESSOURCE_URL: string;

  constructor(base_url: string = "") {
    super();
    console.debug("giverService::constructor", base_url);

    this.RESSOURCE_URL = `${this.BASE_URL}/givers`;
  }

  public async create(value: Giver): Promise<any> {
    return super.POST(this.RESSOURCE_URL, { data: value });
  }
}

export const giverService = new GiverService();
