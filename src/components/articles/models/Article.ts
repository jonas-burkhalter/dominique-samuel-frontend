export default interface Article {
  category: string;
  description: string;
  givers: any;
  id: any;
  image: any;
  price: number;
  slug: string;
  title: string;
}
