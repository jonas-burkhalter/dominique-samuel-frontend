export default interface Article {
  article?: string;
  date?: Date;
  mail?: string;
  name?: string;
  part?: number;
}
