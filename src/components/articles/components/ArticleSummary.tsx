import {
  IonCol,
  IonRow,
} from "@ionic/react";

import Article from "../models/Article";

interface IArticleSummaryComponent {
  article: Article;
}

const ArticleSummaryComponent: React.FC<IArticleSummaryComponent> = (
  props: IArticleSummaryComponent
) => {
  return (
    <IonRow>
      <IonCol sizeSm="3" size="12">
        {props.article?.image?.data !== null && (
          <img
            alt={props.article.image?.data.attributes.alternativeText}
            className="image"
            src={props.article.image?.data.attributes.url}
          />
        )}
      </IonCol>

      <IonCol sizeSm="9" size="12">
        <IonRow>
          <IonCol>
            <h2>{props.article?.title}</h2>
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol size="12">

            <div dangerouslySetInnerHTML={{ __html: props.article?.description || "" }} />
            {/* <div className="description">{props.article?.description}</div> */}
          </IonCol>
        </IonRow>

        {props.article?.price !== null && (
          <IonRow>
            <IonCol sizeSm="6" size="12">
              Preis: {props.article?.price}CHF
            </IonCol>
          </IonRow>
        )}

      </IonCol>
    </IonRow>
  );
};

export default ArticleSummaryComponent; 
