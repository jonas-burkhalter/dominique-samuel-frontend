import {
  IonButton,
  IonCard,
  IonModal,
  IonCol,
  IonRow,
  IonInput,
  IonItem,
  IonLabel,
  useIonToast,
  IonButtons,
  IonHeader,
  IonToolbar,
  IonContent,
} from "@ionic/react";
import { useForm, Controller } from "react-hook-form";

import { giverService } from "../services/GiverService";
import { closeCircle } from "ionicons/icons";

import Article from "../models/Article";
import Giver from "../models/Giver";
import ArticleSummary from "./ArticleSummary";

interface IGiverView {
  article: Article;
  onClose: () => void;
}

const MESSAGE_SUCCESS = "❤️-Dank für dein Geschenk";

const GiverView: React.FC<IGiverView> = (props: IGiverView) => {
  const { control, handleSubmit } = useForm<Giver>();
  const [present, dismiss] = useIonToast();

  function save(data: Giver) {
    data.date = new Date();
    data.article = props.article.id;
    data.part = data.part ? data.part : 1;

    giverService.create(data).then(() => {
      console.log("", data, props.article)

      present({
        buttons: [{ icon: closeCircle, handler: () => dismiss() }],
        color: "success",
        duration: 5000,
        message: MESSAGE_SUCCESS,
      });

      props.onClose();
    })
  }

  return (
    <IonModal isOpen id={props.article.id} onDidDismiss={props.onClose} cssClass="modal-custom">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="end">
            <IonButton onClick={props.onClose}>Schliessen</IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent className="ion-padding">
        <IonCard color="primary">

          <form onSubmit={handleSubmit(save)}>

            <IonRow>
              <IonCol offsetSm="0" sizeSm="12" size="12">
                <ArticleSummary article={props.article} />
              </IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="12">
                <IonItem>
                  <IonLabel position="floating">Name</IonLabel>
                  <Controller
                    name="name"
                    control={control}
                    render={({ field }) => (
                      <IonInput
                        {...field}
                        onIonChange={field.onChange}
                        required
                        type="text"
                      />
                    )}
                  />
                </IonItem>
              </IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="12">
                <IonItem>
                  <IonLabel position="floating">E-Mail</IonLabel>
                  <Controller
                    name="mail"
                    control={control}
                    render={({ field }) => (
                      <IonInput
                        {...field}
                        onIonChange={field.onChange}
                        required
                        type="email"
                      />
                    )}
                  />
                </IonItem>
              </IonCol>
            </IonRow>

            {(props.article.category === 'anteil') && (
              <IonRow>
                <IonCol size="12">
                  <IonItem>
                    <IonLabel position="floating">Betrag</IonLabel>
                    <Controller
                      name="part"
                      control={control}
                      render={({ field }) => (
                        <IonInput
                          {...field}
                          onIonChange={field.onChange}
                          required
                          type="number"
                        />
                      )}
                    />
                  </IonItem>
                </IonCol>
              </IonRow>
            )}

            <IonRow>
              <IonCol size="12">
                <IonButton color="secondary" expand="full" fill="outline" type="submit" >
                  Schenken
                </IonButton>
              </IonCol>
            </IonRow>

          </form>

        </IonCard>
      </IonContent>
    </IonModal>
  );
};

export default GiverView;
