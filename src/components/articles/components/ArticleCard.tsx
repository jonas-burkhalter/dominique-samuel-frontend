import {
  IonButton,
  IonCard,
  IonCol,
  IonRow,
} from "@ionic/react";

import ArticleSummary from "./ArticleSummary"

import Article from "../models/Article";

interface IArticleCardComponent {
  article: Article;
  onOpen: (id: number) => void;
}

const ArticleCardComponent: React.FC<IArticleCardComponent> = (
  props: IArticleCardComponent
) => {
  return (
    <IonCard button color="primary" disabled={props.article.givers?.data.length > 0} onClick={() => props.onOpen(props.article?.id)}>
      <ArticleSummary article={props.article} />

      <IonRow>
        <IonCol offsetSm="6" sizeSm="6" size="12">
          <IonButton color="secondary" expand="full" fill="outline" onClick={() => props.onOpen(props.article?.id)} >
            Schenken
          </IonButton>
        </IonCol>
      </IonRow>
    </IonCard>
  );
};

export default ArticleCardComponent; 
